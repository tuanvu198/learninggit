<?php
$autoloadController = function ($className) {
    $fileName ='App/Controller/'. $className . '.php';
    if (file_exists($fileName)) {
        include_once $fileName;
    }
};

spl_autoload_register($autoloadController);

include_once '/Users/macbook/Desktop/php/Project/Helpers/helper.php';